(function(){

	$(document).ready(function(){

		/* 
		** Carrossel de serviços
		*/
		$('.ncob-plans-carousel').owlCarousel({
			items: 4,
			dots: false,
			nav: false,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,
			smartSpeed: 450,
			margin: 24,
			responsive : {
			    0 : {
			    	items: 1,
			    	mouseDrag: true,
			    	touchDrag: true,
			    	dots: true,
			    },
			    641 : {
			    	items: 2,
			    	mouseDrag: true,
			    	touchDrag: true,
			    	dots: true,
			    },
			    931 : {
			    	items: 3,
			    	mouseDrag: true,
			    	touchDrag: true,
			    	dots: true,
			    },
			    1200 : {
			    	items: 4,
			    }
			}

		});

		/*
		** Abrir e fechar pop up formulário
		** Início
		*/
		$('.pattern-button a').click(function(event){
			event.preventDefault();
			$('.contact-pop-up').addClass('open-pop-up');

			// return;
		});
		$('.contact-pop-up .pop-up .close-pop-up').click(function(){
			$('.contact-pop-up').removeClass('open-pop-up');
		});
		/*
		** Abrir e fechar pop up formulário
		** Fim
		*/

		/*
		** Mostrar vídeo
		*/
		$('.pg-home .about-article figure .play-button').click(function(){
			$(this).hide();
		});

		/*
		** Função para verificar envio do formulário
		** e mostrar mensagem de sucesso
		** Início
		*/
		/*
		$(document).ready(function() {
			$('.wpcf7-submit').click(function(){
				function show_popup(){

					if($("div.wpcf7-validation-errors").length >= 1) {
						$("#modalFormularioError").show();
						clearInterval(robo);
						console.log("Error");
					}
					if($("div.wpcf7-mail-sent-ok").length >= 1) {
						$(".success-message").addClass('display-success-message');
						setTimeout(function(){
							$(".success-message").addClass('show-success-message');
						}, 200);
						console.log("TOp");
					}
				};

				var robo = setInterval( show_popup, 500 );

			});

			$("#modalFormularioError").click(function(){
				$("#modalFormularioError").hide();
			});

			$("#modalFormularioSucess").click(function(){
				$("#modalFormularioSucess").hide();
			});

		});
		*/
		/*
		** Função para verificar envio do formulário
		** e mostrar mensagem de sucesso
		** Fim
		*/

		/* 
		** Função serviços: mostrar conteúdo de cada produto no clique
		** início
		*/
		/*-- desktop e mobile: mostra o conteúdo de cada serviço --*/
		// let liProdutos = document.querySelectorAll('.pg-inicial .secao-servicos .produtos-gerados ul li');
		// let produtos = document.querySelectorAll('.pg-inicial .secao-servicos .conteudo-produtos-gerados');

		// liProdutos.forEach(function(item){
		// 	item.addEventListener('click', function(){
		// 		let dataId = this.getAttribute('data-id');

		// 		liProdutos.forEach(function(item){
		// 			item.classList.remove('produto-ativo');
		// 		});
		// 		this.classList.add('produto-ativo');

		// 		produtos.forEach(function(item){
		// 			item.classList.remove('conteudo-produtos-ativo');
		// 		});

		// 		let produto = produtos[dataId];
		// 		produto.classList.add('conteudo-produtos-ativo');

		// 		if(screen.width <= 768){
		// 			ulProdutos.classList.remove('abrir-menu-produtos-gerados');
		// 			isMenuProdutosOpen = false;
		// 			spanProdutoAtivo.innerText = item.innerText;
		// 		}
		// 	});
		// });
		// document.querySelector('.pg-inicial .secao-servicos .produtos-gerados ul li[data-id="0"]').click();
		
		// /*-- mobile: abre e fecha o menu mobile --*/
		// let spanProdutoAtivo = document.querySelector('.pg-inicial .secao-servicos .produtos-gerados .menu-produtos-gerados .span-produto-ativo');
		// let ulProdutos = document.querySelector('.pg-inicial .secao-servicos .produtos-gerados .menu-produtos-gerados ul');

		// let isMenuProdutosOpen = false;

		// spanProdutoAtivo.addEventListener('click', function(){
		// 	if(!isMenuProdutosOpen){
		// 		ulProdutos.classList.add('abrir-menu-produtos-gerados');
		// 		isMenuProdutosOpen = true;
		// 	} else{
		// 		ulProdutos.classList.remove('abrir-menu-produtos-gerados');
		// 		isMenuProdutosOpen = false;
		// 	}
		// });
		/* 
		** Função serviços
		** fim
		*/
		
		/* 
		** Função áreas de atuação: abrir ou fechar formulário de contato
		** início
		*/
		// let contentFormulario = document.getElementById('formularioFaleConosco');
		// contentFormulario.remove();
		// $('.pg-inicial .secao-areas-atuacao article .areas-atuacao li .span-arrow img').click(function(){
		// 	if(screen.width > 670){
		// 		let thisLi = $(this).parent().parent();

		// 		$('.pg-inicial .secao-areas-atuacao article .areas-atuacao li').removeClass('active-li');
		// 		thisLi.addClass('active-li');

		// 		$('.pg-inicial .secao-areas-atuacao article .areas-atuacao li .container-formulario').removeClass('open-formulario');

		// 		let containerFormulario = thisLi.children('.container-formulario');
		// 		containerFormulario.append(contentFormulario);

		// 		containerFormulario.addClass('open-formulario');
		// 		setTimeout(function(){
		// 			contentFormulario.classList.add('show-formulario');
		// 		}, 250);
		// 	}
		// });

		// $('.pg-inicial .secao-areas-atuacao article .areas-atuacao li').mouseover(function(){
		// 	if(screen.width > 500){
		// 		$('.secao-areas-atuacao #formularioFaleConosco .fechar-formulario img').click(function(){
		// 			let containerFormulario = $(this).parent().parent().parent();

		// 			contentFormulario.classList.remove('show-formulario');
		// 			setTimeout(function(){
		// 				containerFormulario.removeClass('open-formulario');
		// 				$('.pg-inicial .secao-areas-atuacao article .areas-atuacao li').removeClass('active-li');
		// 				contentFormulario.remove();
		// 			}, 250);

		// 		});
		// 	}
		// });
		/* 
		** Função áreas de atuação: abrir ou fechar formulário de contato
		** fim
		*/

		$('.scrollTop a').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
			
		});

		/* 
		** Funções de vídeo
		** início
		*/
		let video = document.querySelector('.pg-home .about-article figure video');

		let playButton = document.querySelector('.pg-home .about-article figure .play-button');
		// let pauseButton = document.querySelector('.pg-inicial .secao-destaque .conteudo-destaque article .div-icones-video .icone-pause-video img');
		// let muteButton = document.querySelector('.pg-inicial .secao-destaque .conteudo-destaque article .div-icones-video .icone-mute-video img');

		playButton.addEventListener('click', function(){
			video.play();
			video.setAttribute('controls', '');
			playButton.style.display = 'none';
			// playButton.src = 'img/playb.svg';
			// pauseButton.src = 'img/pausew.svg';
		});
		// pauseButton.addEventListener('click', function(){
		// 	video.pause();
		// 	pauseButton.src = 'img/pauseb.svg';
		// 	playButton.src = 'img/playw.svg';
		// });
		// muteButton.addEventListener('click', function(){
		// 	if(video.muted){
		// 		video.muted = false;
		// 		muteButton.src = 'img/mute.svg';
		// 	} else{
		// 		video.muted = true;
		// 		muteButton.src = 'img/sound.svg';
		// 	}
		// });
		/* 
		** Funções de vídeo
		** fim
		*/

		/* 
		** Função abrir e fechar pop up
		** início
		*/
		// let body = document.querySelector('body');

		// let buttonSaibaMais = document.querySelector('.pg-inicial .secao-destaque .conteudo-destaque article .div-descricao a');
		// let popUp = document.querySelector('.div-pop-up-formulario');
		// let popUpFormulario = document.querySelector('.div-pop-up-formulario .container-pop-up .container-formulario');

		// buttonSaibaMais.addEventListener('click', function(event){
		// 	event.preventDefault();
		// 	popUp.classList.add('abrir-pop-up');

		// 	popUpFormulario.append(contentFormulario);
		// 	contentFormulario.classList.add('show-formulario');

		// 	body.classList.add('stopScroll');
		// });
		// popUp.addEventListener('mouseover', function(){
		// 	let closePopUP = document.querySelector('.div-pop-up-formulario.abrir-pop-up #formularioFaleConosco .fechar-formulario img');

		// 	closePopUP.addEventListener('click', function(){
		// 		popUp.classList.remove('abrir-pop-up');

		// 		setTimeout(function(){
		// 			contentFormulario.classList.remove('show-formulario');
		// 			contentFormulario.remove();
		// 			body.classList.remove('stopScroll');
		// 		}, 300);
		// 	});
		// });
		/* 
		** Função abrir e fechar pop up
		** fim
		*/

	});

}());